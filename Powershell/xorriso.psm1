Set-Variable -Name "Version" -value 1 -scope Script -option ReadOnly

function GetLocalCygwinBuild {
    # The zip requires the following specified dlls at the specified paths in the zip
    # These dlls can be found in the zips listed on the same line 
    # All of which can be found at https://mirrors.kernel.org/sourceware/cygwin/x86_64/release/:

    # cygwin/cygwin-3.5.1-1.tar.xz - usr/bin/cygwin1.dll
    # libiconv/libiconv2/libiconv2-1.17-1.tar.xz - usr/bin/cygiconv-2.dll
    # zlib/zlib0/zlib0-1.3.1-1.tar.zst - usr/bin/cygz.dll
    
    # Finally the zip needs to contain usr/bin/xorriso.exe
    # which can be found in https://gitlab.com/dittyape/cygwin-geniso/-/raw/main/xorriso-1.5.6.pl02-1.tar.xz
    # If you do not trust my build follow the instructions in 
    # `https://gitlab.com/dittyape/cygwin-geniso/-/blob/main/README.md` to build your own version
    # This may be usefull if xorriso needs to be updated to a newer version.


    # TODO add zip request box.
    Expand-Archive -LiteralPath $zipFile -DestinationPath $CygwinDownloadLocation
    return $zipFile
}

function GetRemoteCygwinBuild{
    $Url = "https://gitlab.com/dittyape/vm-setup/-/raw/main/CreateVM/xorriso.zip?ref_type=heads"
    $outputFile = Join-Path -Path $CygwinDownloadLocation -ChildPath "xorriso.zip"
    Invoke-WebRequest -Uri $Url -OutFile $outputFile
    Expand-Archive -LiteralPath $outputFile -DestinationPath $CygwinDownloadLocation
    return $outputFile
}
function GetXorriso {
    if(-not(Test-Path -Path $xorriso)){
        $download = Read-Choice 'Prebuilt' '&yes','&no' '&yes' 'download xorriso from my zip?'
        switch($download) {
            '&yes' {
                Get-RemoteCygwinBuild
            }
            '&no' {
                Get-LocalCygwinBuild
            }
        }
    }
}
