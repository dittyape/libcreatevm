Set-Variable -Name "Version" -value 1 -scope Script -option ReadOnly
function Get-KaliFile
{
<#
.Synopsis
Downloads file from mirror

.Description
Downloads file from mirror

.Parameter file
File to download from mirror.

.Parameter downloadDir
Dir to save downloaded file.

.Parameter mirror
Mirror url which contains kali files.


.OUTPUTS
System.String. Path to downloaded file 

.Example
$SHA256SUMSPath = Get-KaliFile "SHA256SUMS" $downloadDir $mirror
#>
    param (
        [parameter(Mandatory = $true)] [string] $file,
        [parameter(Mandatory = $true)] [string] $downloadDir,
        [parameter(Mandatory = $true)] [string] $mirror
    )
    $url = -join($mirror, $file)
    $outputFile = Join-Path -Path $downloadDir -ChildPath $file
    Invoke-WebRequest -Uri $url -OutFile $outputFile
    return $outputFile
}

function Get-LatestKali(
        [parameter(Mandatory = $true)] [string] $downloadDir,
        [string] $mirror = "https://cdimage.kali.org/current/" 
    )
{
<#
.Synopsis
Downloads latest version of kali.

.Description
Downloads the sha256sums
Uses the sha256sums to build latest file name.
Checks downloaded version matched hash, (if pre existing file matches hash does not redownload)

.Parameter downloadDir
Dir to save downloaded file.

.Parameter mirror
Mirror url which contains kali files. defualts to "https://cdimage.kali.org/current/".

.OUTPUTS
None 

.Example
Get-LatestKali $TempDownloadLocation
#>
    param 
    $SHA256SUMSPath = Get-KaliFile "SHA256SUMS" $downloadDir $mirror
    $netinstmatches = Select-String -path $SHA256SUMSPath -Pattern '^(?<sha256>[0-9a-fA-F]{64})\s\s(?<filename>kali-linux-(?<version>\d\d\d\d\.\d)-installer-netinst-amd64\.iso)$'
    if($netinstmatches -eq $null){
        Write-Information "Bad SHA256SUMS file" -InformationAction Continue
        exit
    }
    $version = -join("Kali ", $netinstmatches.Matches[0].groups["version"].value)
    $SHA256 = $netinstmatches.Matches[0].groups["sha256"].value
    $kali_file = $netinstmatches.Matches[0].groups["filename"].value
    $kali_file_local = Join-Path -Path $TempDownloadLocation -ChildPath $kali_file
    if(Test-Path -Path $kali_file_local){ 
        # We have iso predownloaded.
        # Check the hash if invalid redownload
        #  Else return
        if((Get-FileHash $kali_file_local -Algorithm SHA256).Hash -eq $SHA256){ 
            return $kali_file_local, $version
        }
    }
    $iso = Get-KaliFile $netinstmatches.Matches[0].groups["filename"].value $downloadDir $mirror
    if((Get-FileHash $kali_file_local -Algorithm SHA256).Hash -eq $SHA256){ 
        return $kali_file_local, $version
    }else{
        Write-Information "kali ISO SHA256 mismatch" -InformationAction Continue
        exit
    }
}
Export-ModuleMember -Function Get-LatestKali 