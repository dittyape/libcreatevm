Set-Variable -Name "Version" -value 1 -scope Script -option ReadOnly
Set-Variable -Name "defaultPreseed" -value @'
################################
# Custom preseed configuration #
################################

# Network
d-i netcfg/get_hostname string kali
d-i netcfg/get_domain string unassigned-domain
d-i netcfg/choose_interface select eth0
d-i netcfg/dhcp_timeout string 60

# Locale
d-i debian-installer/locale string en_GB.UTF-8
d-i console-keymaps-at/keymap select gb
d-i keyboard-configuration/xkb-keymap select gb

# Timezone
d-i clock-setup/utc boolean true
d-i time/zone string Europe/London

# Don't ask for proxy settings
d-i mirror/http/proxy string


## User account
#d-i passwd/user-fullname string [User's Name(can include spaces)]
#d-i passwd/username string [User name no space]
## Normal user's password, either in clear text
#d-i passwd/user-password password [Plain Pass]
#d-i passwd/user-password-again password [Plain Pass]
## or encrypted using a crypt(3) hash.
#d-i passwd/user-password-crypted password [crypt(3) hash]
d-i passwd/user-fullname string kaliuser
d-i passwd/username string kaliuser
## Normal user's password, either in clear text
d-i passwd/user-password password toor
d-i passwd/user-password-again password toor
## or encrypted using a crypt(3) hash.
#d-i passwd/user-password-crypted password [crypt(3) hash]

# Partitioning

# The presently available methods are:
# - regular: use the usual partition types for your architecture
# - lvm:     use LVM to partition the disk
# - crypto:  use LVM within an encrypted partition
d-i partman-auto/method string lvm

# You can define the amount of space that will be used for the LVM volume
# group. It can either be a size with its unit (eg. 20 GB), a percentage of
# free space or the 'max' keyword.
d-i partman-auto-lvm/guided_size string max

# You can choose one of the three predefined partitioning recipes:
# - atomic: all files in one partition
# - home:   separate /home partition
# - multi:  separate /home, /var, and /tmp partitions
d-i partman-auto/choose_recipe select atomic

d-i partman-basicfilesystems/no_swap boolean false
d-i partman-auto-lvm/new_vg_name string kali-vg


# This makes partman automatically partition without confirmation, provided
# that you told it what to do using one of the methods above.
d-i partman-partitioning/confirm_write_new_label boolean true
d-i partman/choose_partition select finish
d-i partman/confirm boolean true
d-i partman/confirm_nooverwrite boolean true

d-i partman-lvm/confirm_nooverwrite boolean true

# Packages
tasksel tasksel/first multiselect standard
d-i pkgsel/include string \
curl git kali-linux-core kali-desktop-xfce python3 git

# Grub
d-i grub-installer/only_debian boolean true
d-i grub-installer/with_other_os boolean false
d-i grub-installer/bootdev string /dev/sda

# Automatically reboot after installation
d-i finish-install/reboot_in_progress note

# Eject media after installation
d-i cdrom-detect/eject boolean true
'@ -scope Script -option ReadOnly 


Set-Variable -Name "defaultPostinstall" -value  @'
# Get installer user
installeruser=$(ls /home)
# Setup desktop
su $installeruser --command "git clone https://gitlab.com/dittyape/vm-setup.git ~/Desktop/vm-setup; echo \"Please run 'cd ~/Desktop/vm-setup/RunInVm/Kali; bash ./setup.sh' on first login\" > ~/Desktop/README.md"
# Setup autologin
sed -i.bak "s/#autologin-user=/autologin-user=$installeruser/g" /etc/lightdm/lightdm.conf
#sleep 500000
'@ -scope Script -option ReadOnly 

function Copy-Directories 
{
<#
.Synopsis
Copies the content of one directory to another.

.Description
Copies the content of one directory to another.

.Parameter source
The source Directory path

.Parameter destination
The destination Directory path

.OUTPUTS
None. Writes to terminal on error

.Example
Copy-Directories (-join((Get-Volume -DiskImage $DiskImage).DriveLetter,':\')) (-join($destinationFolder,'\'))
#>
    param (
        [parameter(Mandatory = $true)] [string] $source,
        [parameter(Mandatory = $true)] [string] $destination        
    )
    # https://stackoverflow.com/questions/11424582/powershell-copy-item-all-files-from-harddrive-to-another-harddrive
    try
    {
        # Create the sub-directories in dest 
        Get-ChildItem -Path $source -Recurse -Force |
            Where-Object { $_.psIsContainer } |
            ForEach-Object { $_.FullName -replace [regex]::Escape($source), $destination } |
            ForEach-Object { $null = New-Item -ItemType Container -Path $_ }
        # Copy the files to dest.
        Get-ChildItem -Path $source -Recurse -Force |
            Where-Object { -not $_.psIsContainer } |
            Copy-Item -Force -Destination { $_.FullName -replace [regex]::Escape($source), $destination }
    }
    catch
    {
        Write-Host "$_"
    }
}

function Set-ReadOnly
{
<#
.Synopsis
Sets or unsets the ReadOnly flag recursively.

.Description
Sets or unsets the ReadOnly flag recursively.
BUG1: native-powershell variant fails so using CMD variant
BUG2: method only removes ATTRIB flag

.Parameter folderPath
The directory path to recursively apply permission

.Parameter setRoAttributeTo
Wether to set or unset readonly flag (Currently always $false and ignored due to BUG1)

.OUTPUTS
None. 

.Example
Set-ReadOnly $destinationFolder $false
#>

    param (
        [parameter(Mandatory = $true)] [string] $folderPath,
        [parameter(Mandatory = $true)] [System.boolean] $setRoAttributeTo 
    )
    #-r==remove read only; /s==recursive
    attrib -r (-join($folderPath,'\*')) /s

    ## The following fails to remove flag so using CMD variant above to remove readonly flag
    #Get-ChildItem -Recurse -Path $folderPath | foreach {
    #    Write-Information "$_.FullName $setRoAttributeTo" -InformationAction Continue
    #    Set-ItemProperty -Path $_.FullName -Name IsReadOnly -Value $setRoAttributeTo
    #    Get-ItemProperty -Path $_.FullName -Name IsReadOnly
    #}
}

function Read-Iso
{
<#
.Synopsis
Extracts given Iso to folder.

.Description
Extracts given Iso to folder, by mounting and copying from the ISO
Also removes Readonly Flag on all files to allow patching.

.Parameter sourceIso
Iso to extract

.Parameter destinationFolder
Folder to extract to.

.OUTPUTS
None. 

.Example
Read-Iso $srcIso $TempDir
#>
    param (
        [parameter(Mandatory = $true)] [string] $sourceIso,
        [parameter(Mandatory = $true)] [string] $destinationFolder        
    )
    $DiskImage = Mount-DiskImage -ImagePath $sourceIso -PassThru
    Copy-Directories (-join((Get-Volume -DiskImage $DiskImage).DriveLetter,':\')) (-join($destinationFolder,'\'))
    Set-ReadOnly $destinationFolder $false
    Dismount-DiskImage -DevicePath $DiskImage.DevicePath
}

function New-KaliIso
{
<#
.Synopsis
Builds new ISO based on patched folder.

.Description
Builds new ISO based on patched folder.
BUG: does not keep usb boot :/ but UEFI and BIOS boot from CD works.

.Parameter srcFolder
Kali extracted and patched source folder.

.Parameter destIso
ISO file to write to.

.Parameter xorriso
path to xorriso.exe to use to make iso.

.OUTPUTS
None. 

.Example
New-KaliIso $TempDir $destIso
#>
    param (
        [parameter(Mandatory = $true)] [string] $srcFolder,
        [parameter(Mandatory = $true)] [string] $destIso,
        [parameter(Mandatory = $true)] [string] $xorriso      
    )
    Push-Location $srcFolder
    # dd if="$orig_iso" bs=1 count=432 of="syslinux/usr/lib/ISOLINUX/isohdpfx.bin"
    #& $xorriso -as mkisofs -r -V 'Kali Linux amd64 n' -o $destIso -J -joliet-long -cache-inodes -isohybrid-mbr syslinux/usr/lib/ISOLINUX/isohdpfx.bin -b isolinux/isolinux.bin -c isolinux/boot.cat -boot-load-size 4 -boot-info-table -no-emul-boot -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus .
    & $xorriso -as mkisofs -r -V 'Kali Linux amd64 n' -o $destIso -J -joliet-long -cache-inodes -b isolinux/isolinux.bin -c isolinux/boot.cat -boot-load-size 4 -boot-info-table -no-emul-boot -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus .
    Pop-Location
}

function GrubCFGGenerator
{
<#
.Synopsis
Patches boot cfg files to enable autoinstall and auto start install after timeout.

.Description
Reads the config file.
Updates the config to enable auto install and select the simple-cdd profile named kali.
Also changes the defualt menu selection from speech synthesis to normal graphical install.

.Parameter cfgFile
Config file to update.

.OUTPUTS
None. 

.Example
GrubCFGGenerator $GrubCfgFile
#>
    param (
        [parameter(Mandatory = $true)] [string] $cfgFile
    )
    # Using default over kali as then kali.postinstall will auto run with no issue.
    $content = [System.IO.File]::ReadAllText($cfgFile).Replace(
        "preseed/file=/cdrom/simple-cdd/default.preseed",
        "auto=true preseed/file=/cdrom/simple-cdd/default.preseed"
    ).Replace(
        "ontimeout /install.amd/vmlinuz desktop=xfce vga=788 initrd=/install.amd/gtk/initrd.gz speakup.synth=soft --- quiet",
        "ontimeout /install.amd/vmlinuz auto=true preseed/file=/cdrom/simple-cdd/default.preseed simple-cdd/profiles=kali desktop=xfce vga=788 initrd=/install.amd/gtk/initrd.gz --- quiet"
    ).Replace(
        "menu autoboot Press a key, otherwise speech synthesis will be started in # second{,s}...",
        "menu autoboot Press a key, otherwise install will be started in # second{,s}..."
    )
    [System.IO.File]::WriteAllText($cfgFile, $content)
}

function Update-GrubCFG
{
<#
.Synopsis
Patches boot cfg files to enable autoinstall and auto start install after timeout.

.Description
Reads the config files for ISOLinux and grub.
Updates the configs to enable auto install and select the simple-cdd profile named kali.
Also changes the defualt menu selection from speech synthesis to normal graphical install.

.Parameter isoFolder
Folder containing extracted kali iso to patch.

.OUTPUTS
None. 

.Example
Update-GrubCFG $TempDir
#>
    param (
        [parameter(Mandatory = $true)] [string] $isoFolder
    )
    # Update grubmenu items
    $GrubCfgFile = Join-Path -Path $isoFolder -ChildPath ".\boot\grub\grub.cfg"
    GrubCFGGenerator $GrubCfgFile
    # Update BIOS/isolinux items
    $isolinux = Join-Path -Path $isoFolder -ChildPath ".\isolinux\"
    Get-ChildItem $isolinux -Filter *.cfg | Foreach-Object { GrubCFGGenerator $_.FullName }
}

function Update-KaliFile
{
<#
.Synopsis
Updates specified kali file in the isofolder.

.Description
Updates specified kali file in the isofolder.

.Parameter isoFolder
Root of the extracted iso.

.Parameter kaliFile
File in Iso to update.

.Parameter content
Content of the file to save.

.OUTPUTS
None. 

.Example
Update-KaliFile $TempDir ".\simple-cdd\default.preseed" $preseedContent
#>
    param (
        [parameter(Mandatory = $true)] [string] $isoFolder,
        [parameter(Mandatory = $true)] [string] $kaliFile,
        [parameter(Mandatory = $true)] [string] $content,

    )

    $file = Join-Path -Path $isoFolder -ChildPath $kaliFile
    $additions=$content.Replace("`r`n","`n") # Change to linux line endings
    Add-Content -Path $file -Value $additions -NoNewline
}


function PatchIso
{
<#
.Synopsis
Patchs a kali iso for auto install.

.Description
Extracts a given iso. Enables auto install and changes the timeout option to the auto install.
Adds presees content and post install content as provided/defaulted to.

.Parameter srcIso
source iso path.

.Parameter destIso
output iso path.

.Parameter xorriso
path to xorriso.exe to use to make iso.

.Parameter workingDir
directory to use as the working location.

.Parameter preseedContent
Allows overriding of the defualt preseed content provided by this lib.

.Parameter postinstallContent
Allows overriding of the defualt post content provided by this lib.

.OUTPUTS
None. 

.Example
PatchIso $iso_in $iso_out $xorriso
#>
    param (
        [parameter(Mandatory = $true)] [string] $srcIso,
        [parameter(Mandatory = $true)] [string] $destIso,
        [parameter(Mandatory = $true)] [string] $xorriso, 
        [string] $workingDir = ([System.IO.Path]::GetTempPath()),
        [string] $preseedContent = $defaultPreseed,
        [string] $postinstallContent = $defaultPostinstall,
    )
    # Create Temp dir
    $TempDir = New-Item -ItemType Directory -Path (Join-Path -Path $workingDir -ChildPath "isoWorkingDir")
    try{
        Read-Iso $srcIso $TempDir
        Update-GrubCFG $TempDir
        Update-KaliFile $TempDir ".\simple-cdd\default.preseed" $preseedContent
        Update-KaliFile $TempDir ".\simple-cdd\default.postinst" $postinstallContent
        New-KaliIso $TempDir $destIso $xorriso
    }finally{
        # clean Temp dir
        Remove-Item -Path $TempDir -Recurse -Force 
    }
}

Export-ModuleMember -Function PatchIso 
Export-ModuleMember -Variable Version