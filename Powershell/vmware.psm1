Set-Variable -Name "Version" -value 1 -scope Script -option ReadOnly

Set-Variable -Name "ProgramFilesx86" -value ([Environment]::GetEnvironmentVariable("ProgramFiles(x86)")) -scope Script -option ReadOnly
Set-Variable -Name "VMwarePro" -value (Join-Path -Path $ProgramFilesx86 -ChildPath "VMware\VMware Workstation") -scope Script -option ReadOnly
Set-Variable -Name "vmware_vdiskmanager" -value (Join-Path -Path $VMwarePro -ChildPath "vmware-vdiskmanager.exe") -scope Script -option ReadOnly
Set-Variable -Name "vmrun" -value (Join-Path -Path $VMwarePro -ChildPath "vmrun.exe") -scope Script -option ReadOnly

function GetVMPath{
<#
.Synopsis
Creates Path For VM.

.Description
Asks user for name to call VM. Then Creates a dir for the VM in the "MyDocuments/Virtual Machines" folder.
Also Checks if the dir already exists and if so asks for a new name.

.Parameter defaultName
Name to default to if the user does not provide a name

.OUTPUTS
System.IO.DirectoryInfo. Path to created VM folder.
System.String. Name of created provided VM

.Example
$VMpath, $VMname = Get-VM-Path "Kali vm"
#>
    param(
        [parameter(Mandatory = $true)] [string] $defaultName
    )
    $VMname = Read-Host -Prompt "VM Name [$defaultName]"
    do 
    {
        if ([string]::IsNullOrWhiteSpace($VMname))
        {
            $VMname = $defaultName
        }
        $VMdirs = Join-Path -Path ([Environment]::GetFolderPath("MyDocuments")) -ChildPath "Virtual Machines"
        $VMpath = Join-Path -Path  $VMDirs -ChildPath $VMname
        if(Test-Path -Path $VMpath){
            $VMname = Read-Host -Prompt "VM already exists with that name! VM Name [$defaultName]"
        }  
    } while(Test-Path -Path $VMpath)
    $VMpath = New-Item -ItemType Directory -Path $VMpath -Force
    return $VMpath, $VMname
}

function CreateDisk
{
<#
.SYNOPSIS
Creates a New VM Disk image

.Description
Calls VM disk manager to create a split sparse disk.
The disk will be saved in the VMpath folder and named after the VMname.
The size of the disk will either be the size passed or "100GB" if not set.

.Parameter VMpath
Name to default to if the user does not provide a name

.Parameter VMname
VM name, used as the file name, with the vmdk extension

.Parameter VMsize
Size of the VM, defaults to "100GB"

.OUTPUTS
System.String. Path to VM disk

.Example
$diskPath = Create-disk $VMpath $VMname
#>
    param ( 
        [parameter(Mandatory = $true)] [string] $VMpath,
        [parameter(Mandatory = $true)] [string] $VMname,
        [string] $VMsize = "100GB",
    )
    # Create Disk
    ## -c Creates a local v disk
    ## -a Specifies the disk adapter type
    ## -s size of disk
    ## -t Specifies the virtual disk type.
    ###    0 - create a growable virtual disk contained in a single file (monolithic sparse)
    ###    1 - create a growable virtual disk split into 2GB files (split sparse)
    ###    2 - create a preallocated virtual disk contained in a single file (monolithic flat)
    ###    3 - create a preallocated virtual disk split into 2GB files (split flat).
    ###    4 - create a preallocated virtual disk compatible with ESX server (VMFS flat).
    ###    5 - create a compressed disk optimized for streaming
    $diskName = -Join($VMname, ".vmdk")
    $diskPath = Join-Path -Path $VMpath -ChildPath $diskName
    Write-Information (& $vmware_vdiskmanager -c -a lsilogic -s $VMsize -t 1 $diskPath) -InformationAction Continue
    return $diskPath
}

function CreateVM {
<#
.SYNOPSIS
Creates a VMware vmx file from template

.Description


.Parameter VMpath
Name to default to if the user does not provide a name

.Parameter VMname
VM name, used as the file name, with the vmdk extension

.Parameter VMiso
Iso which the VM will boot from

.Parameter VMdiskPath


.Parameter VMguestType
vmware designation for guestOS in vmx file.
Example values:
Windows 10 = "windows9-64"
Kali Linux = "debian12-64"

.OUTPUTS
None

.Example
Create-VM $VMpath $VMname $VMiso $diskPath 
#>
    param (
        [parameter(Mandatory = $true)] [string] $VMpath,
        [parameter(Mandatory = $true)] [string] $VMname,
        [parameter(Mandatory = $true)] [string] $VMiso,
        [parameter(Mandatory = $true)] [string] $VMdiskPath,
        [parameter(Mandatory = $true)] [string] $VMguestType
    )
    $templateName = -Join($VMname, ".vmx")
    $templatePath = Join-Path -Path $VMpath -ChildPath $templateName
    $templateContent=@"
.encoding = "windows-1252"
config.version = "8"
virtualHW.version = "21"
pciBridge0.present = "TRUE"
pciBridge4.present = "TRUE"
pciBridge4.virtualDev = "pcieRootPort"
pciBridge4.functions = "8"
pciBridge5.present = "TRUE"
pciBridge5.virtualDev = "pcieRootPort"
pciBridge5.functions = "8"
pciBridge6.present = "TRUE"
pciBridge6.virtualDev = "pcieRootPort"
pciBridge6.functions = "8"
pciBridge7.present = "TRUE"
pciBridge7.virtualDev = "pcieRootPort"
pciBridge7.functions = "8"
vmci0.present = "TRUE"
hpet0.present = "TRUE"
nvram = "$VMname.nvram"
virtualHW.productCompatibility = "hosted"
powerType.powerOff = "soft"
powerType.powerOn = "soft"
powerType.suspend = "soft"
powerType.reset = "soft"
displayName = "$VMname"
usb.vbluetooth.startConnected = "FALSE"
guestOS = "$VMguestType"
tools.syncTime = "FALSE"
sound.autoDetect = "TRUE"
sound.fileName = "-1"
sound.present = "TRUE"
numvcpus = "8"
cpuid.coresPerSocket = "1"
vcpu.hotadd = "TRUE"
memsize = "16384"
scsi0.virtualDev = "lsilogic"
scsi0.present = "TRUE"
scsi0:0.fileName = "$VMname.vmdk"
scsi0:0.present = "TRUE"
ide1:0.deviceType = "cdrom-image"
ide1:0.fileName = "$VMiso"
ide1:0.present = "TRUE"
usb.present = "TRUE"
ehci.present = "TRUE"
usb_xhci.present = "TRUE"
ethernet0.connectionType = "nat"
ethernet0.addressType = "generated"
ethernet0.virtualDev = "e1000"
ethernet0.present = "TRUE"
extendedConfigFile = "$VMname.vmxf"
floppy0.present = "FALSE"    
"@
    Set-Content -Path $templatePath -Value $templateContent
    #TODO: Add Optional fix for slow vm on Recent Intel CPUs 
}

function StartVM {
    param (
        [parameter(Mandatory = $true)] [string] $VMpath,
        [parameter(Mandatory = $true)] [string] $VMname
    )
    $templateName = -Join($VMname, ".vmx")
    $templatePath = Join-Path -Path $VMpath -ChildPath $templateName
    & $vmrun start $templatePath
}

function CreateStartVM {
<#
.Synopsis
Creates and starts a VM with the given ISO.

.Description
Asks user for name to call VM defualting to defaultName.
Then Creates a dir for the VM in the "MyDocuments/Virtual Machines" folder.
Also Checks if the dir already exists and if so asks for a new name.
Following this creates a disk for the VM.
Then Creates the VM template VMX file
Following this will open VMware and starts the VM

.Parameter VMiso
Iso which the VM should boot from.

.Parameter defaultName
Name to default to if the user does not provide a name

.Parameter VMguestType
vmware designation for guestOS in vmx file.
Example values:
Windows 10 = "windows9-64"
Kali Linux = "debian12-64"

.OUTPUTS
None

.Example
Create-Start-VM $iso_out $iso_version
#>
    param (
        [parameter(Mandatory = $true)] [string] $VMiso,
        [parameter(Mandatory = $true)] [string] $defaultName,
        [parameter(Mandatory = $true)] [string] $VMguestType
    )
    $VMpath, $VMname = Get-VM-Path $defaultName
    $diskPath = Create-disk $VMpath $VMname
    Create-VM $VMpath $VMname $VMiso $diskPath $VMguestType
    Start-VM $VMpath $VMname
}

function TestHypervisor{
<#
.Synopsis
Checks Hypervisor is in correct state.
#>
return $false

}