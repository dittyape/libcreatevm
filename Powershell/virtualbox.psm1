Set-Variable -Name "Version" -value 1 -scope Script -option ReadOnly
throw "Methods not implemented!!!"

function GetVMPath{
<#
.Synopsis
Creates Path For VM.

.Description
Asks user for name to call VM. Then Creates a dir for the VM in the "MyDocuments/Virtual Machines" folder.
Also Checks if the dir already exists and if so asks for a new name.

.Parameter defaultName
Name to default to if the user does not provide a name

.OUTPUTS
System.IO.DirectoryInfo. Path to created VM folder.
System.String. Name of created provided VM

.Example
$VMpath, $VMname = Get-VM-Path "Kali vm"
#>
    param(
        [parameter(Mandatory = $true)] [string] $defaultName
    )
    $VMname = Read-Host -Prompt "VM Name [$defaultName]"
    do 
    {
        if ([string]::IsNullOrWhiteSpace($VMname))
        {
            $VMname = $defaultName
        }
        $VMdirs = Join-Path -Path ([Environment]::GetFolderPath("MyDocuments")) -ChildPath "Virtual Machines"
        $VMpath = Join-Path -Path  $VMDirs -ChildPath $VMname
        if(Test-Path -Path $VMpath){
            $VMname = Read-Host -Prompt "VM already exists with that name! VM Name [$defaultName]"
        }  
    } while(Test-Path -Path $VMpath)
    $VMpath = New-Item -ItemType Directory -Path $VMpath -Force
    return $VMpath, $VMname
}

function CreateDisk
{
<#
.SYNOPSIS
Creates a New VM Disk image

.Description
Calls VM disk manager to create a split sparse disk.
The disk will be saved in the VMpath folder and named after the VMname.
The size of the disk will either be the size passed or "100GB" if not set.

.Parameter VMpath
Name to default to if the user does not provide a name

.Parameter VMname
VM name, used as the file name, with the vmdk extension

.Parameter VMsize
Size of the VM, defaults to "100GB"

.OUTPUTS
System.String. Path to VM disk

.Example
$diskPath = Create-disk $VMpath $VMname
#>
    param ( 
        [parameter(Mandatory = $true)] [string] $VMpath,
        [parameter(Mandatory = $true)] [string] $VMname,
        [string] $VMsize = "100GB",
    )
    $diskName = -Join($VMname, ".vmdk")
    $diskPath = Join-Path -Path $VMpath -ChildPath $diskName
    #Write-Information (& $vmware_vdiskmanager -c -a lsilogic -s $VMsize -t 1 $diskPath) -InformationAction Continue
    return $diskPath
}

function CreateVM {
<#
.SYNOPSIS
Creates a VMware vmx file from template

.Description


.Parameter VMpath
Name to default to if the user does not provide a name

.Parameter VMname
VM name, used as the file name, with the vmdk extension

.Parameter VMiso
Iso which the VM will boot from

.Parameter VMdiskPath


.Parameter VMguestType


.OUTPUTS
None

.Example
Create-VM $VMpath $VMname $VMiso $diskPath 
#>
    param (
        [parameter(Mandatory = $true)] [string] $VMpath,
        [parameter(Mandatory = $true)] [string] $VMname,
        [parameter(Mandatory = $true)] [string] $VMiso,
        [parameter(Mandatory = $true)] [string] $VMdiskPath,
        [parameter(Mandatory = $true)] [string] $VMguestType
    )
    
}

function StartVM {
    param (
        [parameter(Mandatory = $true)] [string] $VMpath,
        [parameter(Mandatory = $true)] [string] $VMname
    )
    #$templateName = -Join($VMname, ".vmx")
    #$templatePath = Join-Path -Path $VMpath -ChildPath $templateName
    #& $vmrun start $templatePath
}

function CreateStartVM {
<#
.Synopsis
Creates and starts a VM with the given ISO.

.Description
Asks user for name to call VM defualting to defaultName.
Then Creates a dir for the VM in the "MyDocuments/Virtual Machines" folder.
Also Checks if the dir already exists and if so asks for a new name.
Following this creates a disk for the VM.
Then Creates the VM template VMX file
Following this will open VMware and starts the VM

.Parameter VMiso
Iso which the VM should boot from.

.Parameter defaultName
Name to default to if the user does not provide a name

.Parameter VMguestType


.OUTPUTS
None

.Example
Create-Start-VM $iso_out $iso_version
#>
    param (
        [parameter(Mandatory = $true)] [string] $VMiso,
        [parameter(Mandatory = $true)] [string] $defaultName,
        [parameter(Mandatory = $true)] [string] $VMguestType
    )
    $VMpath, $VMname = Get-VM-Path $defaultName
    $diskPath = Create-disk $VMpath $VMname
    Create-VM $VMpath $VMname $VMiso $diskPath $VMguestType
    Start-VM $VMpath $VMname
}

function TestHypervisor{
<#
.Synopsis
Checks Hypervisor is in correct state.
#>
return $false

}